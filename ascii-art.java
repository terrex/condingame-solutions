import java.util.*;
import java.io.*;
import java.math.*;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
class Solution {

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int L = in.nextInt();
        int H = in.nextInt();
        if (in.hasNextLine()) {
            in.nextLine();
        }
        String T = in.nextLine().toUpperCase();
        String[] map = new String[H];
        String[] output = new String[H];
        String allLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ?";
        for (int i = 0; i < H; i++) {
            String ROW = in.nextLine();
            map[i] = ROW;
        }
        
        for(int i = 0; i < T.length(); i++) {
            int index = allLetters.indexOf(T.substring(i, i + 1));
            if(index < 0) index = 26;
            for(int j = 0; j < H; j++) {
                output[j] = ((output[j] == null)?(""):(output[j])) + 
                    map[j].substring(index * L, index * L + L);
            }
        }
        for(int j = 0; j < H; j++) {
            System.out.println(output[j]);
        }
    }
}
